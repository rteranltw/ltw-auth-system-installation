## Installation API
- git clone git@gitlab.com:rteranltw/ltw-core-api.git
- composer install
- php artisan key:generate
- php artisan migrate:refresh --seed


```
INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'PY2xMlvrNsg0TCibweqNTbNSNPUIdIIDA1rlhKfq', 'http://localhost', 1, 0, 0, '2019-07-05 09:48:56', '2019-07-05 09:48:56'),
(2, NULL, 'Laravel Password Grant Client', '20V0H4R57KdNoio1XvqTytRrbFpzj9OqYxu9Tsai', 'http://localhost', 0, 1, 0, '2019-07-05 09:48:56', '2019-07-05 09:48:56');
```

```
INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-07-05 09:48:56', '2019-07-05 09:48:56');
```

## Installation FRONT SPA
- git clone git@gitlab.com:rteranltw/ltw-auth-spa.git
- Change constant apiUrl in files index.html and users.html

## Understanding OAuth2 
- http://www.bubblecode.net/en/2016/01/22/understanding-oauth2/
- https://medium.com/@joelbrubaker/laravel-passport-spa-frontend-authentication-e876cd81928f
- https://stackoverflow.com/questions/24724238/how-do-client-side-js-libraries-for-oauth2-maintain-secure-authentication?noredirect=1&lq=1
- https://stackoverflow.com/questions/54275082/what-are-the-main-difference-between-personal-access-client-and-password-client
- https://www.reddit.com/r/laravel/comments/87r050/laravel_passport_and_mobile_app_api_authentication/
- https://auth0.com/docs/applications/concepts/app-types-first-third-party
- https://stackoverflow.com/questions/48182566/laravel-passport-api-implicit-grant
- https://github.com/laravel/passport/issues/914
- https://codeburst.io/using-ajax-user-login-in-laravel-5-3-5-4-5-5-5-6-d4e30b47985f

### Authorization grant types
OAuth2 defines 4 grant types depending on the location and the nature of the client involved in obtaining an access token.
- Authorization Code Grant
- Implicit Grant
- Resource Owner Password Credentials Grant
- Client Credentials Grant

### Example 1 
The OAuth2 password grant allows your other first-party clients, such as a mobile application, to obtain an access token using an e-mail address / username and password. This allows you to issue access tokens securely to your first-party clients without requiring your users to go through the entire OAuth2 authorization code redirect flow.native device applications.
Repo (ltw-auth-spa)

```
POST --> http://your-url.loc/oauth/token
PARAMS -->
username:rafa@yopmail.com
password:demo
client_id:2
client_secret:20V0H4R57KdNoio1XvqTytRrbFpzj9OqYxu9Tsai
//scope:*
grant_type:password
```

If logged successfylly you should recieve a response similar to this one

```
{
    "token_type": "Bearer",
    "expires_in": 31622400,
    "access_token": "*****",
    "refresh_token": "****"
}
```

This is an example of retreving data from a protected endpoint after having authenticate by passing the 'access_token'
```
GET --> http://your-url.loc/api/users
HEADERS PARANS
Authorization: Bearer 'access_token'
Accept: application/json
```


### Example 2
The user authenticate through the php login form and creates a session. The api stills guarded with passport system so to be able to consum the api we need to add a middleware that will convert our session in api key.
1. http://your-url.loc/login
2. http://your-url.loc/backend
3. http://your-url.loc/backend/users
